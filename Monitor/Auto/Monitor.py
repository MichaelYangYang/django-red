import datetime
import os
import time
import paho.mqtt.client as mqtt
import random
import json
import subprocess

# 讀取設定檔
def Config():
    Dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # print(Dir)
    file_out = open("{}/Config/config.json".format(Dir),"r",newline="")
    config = json.loads(file_out.read())
    file_out.close()
    return config


def start_mqtt():  # 啟動MQTT連線
    # 讀取設定檔
    MQTTConfig = Config()["MQTTServer"]
    User = MQTTConfig["user"]
    Password = MQTTConfig["paswd"]
    Address = MQTTConfig["ip"]
    Port = MQTTConfig["port"]
    DeviceID = Create_DeviceID()  # 隨機創建一組裝置ID
    client = mqtt.Client(DeviceID, False)  # False參數用於記住訂閱的Topic
    client.username_pw_set(User, Password)
    client.connect(Address, Port, keepalive=60)
    client.on_connect = on_connect  # 連線狀態的回調函式
    client.on_message = on_message  # 訂閱有訊息的回調函式
    client.on_disconnect = on_disconnect  # 中斷連線狀態的回調函式
    return client


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))


def on_message(mq, userdata, msg):  # 回調函數，處理訊息內容
    print("{}:{}".format(str(msg.topic), str(msg.payload, encoding="utf-8")))
    topic = str(msg.topic)
    payload = str(msg.payload, encoding="utf-8")
    # 如果是DjangoRed的控制命令
    if topic == "DjangoRed/MonitorService/CMD":
        data = {}
        # 全部更新
        if payload.find("update") == 0:
            status,result = subprocess.getstatusoutput("supervisorctl update")
            data["status"] = status
            data["result"] = result
            print("supervisorctl update")  # 之後改成命令
        # 啟動特定腳本
        if payload.find("start:") == 0:
            ProgramName = payload.split(":")[1]
            status,result = subprocess.getstatusoutput("supervisorctl start {}".format(ProgramName))
            data["status"] = status
            data["result"] = result
            print("supervisorctl start {}".format(ProgramName))  # 之後改成命令
        # 重啟特定腳本
        if payload.find("restart:") == 0:
            ProgramName = payload.split(":")[1]
            status,result = subprocess.getstatusoutput("supervisorctl restart {}".format(ProgramName))
            data["status"] = status
            data["result"] = result
            print("supervisorctl restart {}".format(ProgramName))  # 之後改成命令
        # 暫停特定腳本
        if payload.find("stop:") == 0:
            ProgramName = payload.split(":")[1]
            status,result = subprocess.getstatusoutput("supervisorctl stop {}".format(ProgramName))
            data["status"] = status
            data["result"] = result
            print("supervisorctl stop {}".format(ProgramName))  # 之後改成命令
        # 查詢特定腳本運行狀態
        if payload.find("status:") == 0:
            ProgramName = payload.split(":")[1]
            status,result = subprocess.getstatusoutput("supervisorctl status {}".format(ProgramName))
            data["status"] = status
            data["result"] = result
            print("supervisorctl status {}".format(ProgramName))  # 之後改成命令
            
            
        # 傳給DjangoRed訊息
        client.publish(
            "MonitorService/DjangoRed/MSG",
            payload=json.dumps(data),
            qos=0,
            retain=True,
        )


def on_disconnect(client, userdata, rc):
    print("Connected with result code " + str(rc))
    if rc != 0:  # 當連線中斷時
        print("Unexpected disconnection.")


def Create_DeviceID():  #  產生序號
    TestTime = datetime.datetime.now().strftime("%Y%m%d%H%M")
    randomSTR = "".join(random.sample("zyxwvutsrqponmlkjihgfedcba", 3))
    Serial_Number = "{}{}".format(TestTime, randomSTR)  # 序號
    return Serial_Number


if __name__ == "__main__":
    # 啟用supervisor服務
    subprocess.run("supervisord -c /etc/supervisord.conf", shell=True)

    client = start_mqtt()  # mqtt連線
    client.on_message = on_message  # 重設回調函式
    client.loop_start()
    while True:
        try:
            subscribe_list = [("DjangoRed/MonitorService/CMD", 0)]  # 訂閱資料宣告
            client.subscribe(subscribe_list)  # 批次訂閱
            time.sleep(1)
        except Exception as ee:
            print(str(ee))
