# Date:Wed Oct 07 2020 11:12:27 GMT+0800 (台北標準時間)
# Put code on here...
# -*- coding: utf-8 -*-
"""
Created on Fri May 31 18:06:38 2019

@author: a5233
"""
import datetime
import time
import paho.mqtt.client as mqtt
import random
import pymongo


def start_mqtt():  # 啟動MQTT連線
    User = "admin"
    Password = "a3345678"
    Address = "192.168.132.99"
    Port = 1883
    DeviceID = Create_DeviceID()  # 隨機創建一組裝置ID

    def on_connect(client, userdata, flags, rc):
        print("Connected with result code " + str(rc))

    def on_message(client, userdata, msg):
        print(msg.topic + " " + str(msg.payload))

    def on_disconnect(client, userdata, rc):
        print("Connected with result code " + str(rc))
        if rc != 0:  # 當連線中斷時
            print("Unexpected disconnection.")

    client = mqtt.Client(DeviceID, False)  # False參數用於記住訂閱的Topic
    client.username_pw_set(User, Password)
    client.connect(Address, Port, keepalive=60)
    client.on_connect = on_connect  # 連線狀態的回調函式
    client.on_message = on_message  # 訂閱有訊息的回調函式
    client.on_disconnect = on_disconnect  # 中斷連線狀態的回調函式
    # client.loop_start()#開始迴圈
    return client


def on_message(mq, userdata, msg):  # 回調函數，處理訊息內容
    print("{}:{}".format(str(msg.topic), str(msg.payload, encoding="utf-8")))
    save_data(
        Topic=str(msg.topic), Payload=str(msg.payload, encoding="utf-8")
    )  # 將原始數據存入mongodb


def Create_DeviceID():  #  產生序號
    TestTime = datetime.datetime.now().strftime("%Y%m%d%H%M")
    randomSTR = "".join(random.sample("zyxwvutsrqponmlkjihgfedcba", 3))
    Serial_Number = "{}{}".format(TestTime, randomSTR)  # 序號
    return Serial_Number


def start_mongo():  # 啟用mongodb連線
    client = pymongo.MongoClient(
        "192.168.132.200",  # 資料庫主機Host
        27017,  # 資料庫Port
        username="fhnuser",  # 使用者帳戶
        password="fhn24566673",  # 使用者密碼
        authSource="RawData",  # 身分認證所有庫
        authMechanism="SCRAM-SHA-1",  # 登入認證機制
    )
    return client


def save_data(Topic, Payload):  # 將原始數據存入mongodb
    client = start_mongo()
    db = client["RawData"]
    today = datetime.datetime.now()
    collection = db[today.strftime("%Y%m%d")]
    collection.insert_one({"Topic": Topic, "Time": today, "Payload": Payload})


if __name__ == "__main__":
    client = start_mqtt()  # mqtt連線
    client.on_message = on_message  # 重設回調函式
    client.loop_start()
    while True:
        try:
            subscribe_list = [
                ("iot400/test/data", 0),
                ("wrbkh/fs/3972500FL0001", 0),
                ("wrbkh/fs/3972500FL0002", 0),
                ("wrbkh/fs/3972500FL0003", 0),
                ("wrbkh/fs/3972500FL0004", 0),
                ("wrbkh/fs/3972500FL0005", 0),
                ("wrbkh/fs/3972500FL0006", 0),
                ("wrbkh/fs/3972500FL0007", 0),
                ("wrbkh/fs/3972500FL0008", 0),
                ("wrbkh/fs/3972500FL0009", 0),
                ("wrbkh/fs/3972500FL0010", 0),
                ("wrbkh/fs/3972500FL0011", 0),
                ("wrbkh/fs/3972500FL0012", 0),
                ("wrbkh/fs/3972500FL0013", 0),
                ("wrbkh/fs/3972500FL0014", 0),
                ("wrbkh/fs/3972500FL0015", 0),
                ("wrbkh/fs/3972500FL0016", 0),
                ("wrbkh/fs/3972500FL0017", 0),
                ("wrbkh/fs/3972500FL0018", 0),
                ("wrbkh/fs/3972500FL0019", 0),
                ("wrbkh/fs/3972500FL0020", 0),
                ("wrbkh/fs/3972500FL0021", 0),
                ("wrbkh/fs/3972500FL0022", 0),
                ("wrbkh/fs/3972500FL0023", 0),
                ("wrbkh/fs/3972500FL0024", 0),
                ("wrbkh/fs/3972500FL0025", 0),
                ("wrbkh/fs/3972500FL0026", 0),
                ("wrbkh/fs/3972500FL0027", 0),
                ("wrbkh/fs/3972500FL0028", 0),
                ("wrbkh/fs/3972500FL0029", 0),
                ("wrbkh/fs/3972500FL0030", 0),
                ("wrbkh/fs/3972500FL0031", 0),
                ("wrbkh/fs/3972500FL0032", 0),
                ("wrbkh/fs/3972500FL0033", 0),
                ("wrbkh/fs/3972500FL0034", 0),
                ("wrbkh/fs/3972500FL0035", 0),
                ("wrbkh/fs/3972500FL0036", 0),
                ("wrbkh/fs/3972500FL0037", 0),
                ("wrbkh/fs/3972500FL0038", 0),
                ("wrbkh/fs/3972500FL0039", 0),
                ("wrbkh/fs/3972500FL0040", 0),
                ("wrbkh/fs/3972500FL0041", 0),
                ("wrbkh/fs/3972500FL0042", 0),
                ("wrbkh/fs/3972500FL0043", 0),
                ("wrbkh/fs/3972500FL0044", 0),
                ("wrbkh/fs/3972500FL0045", 0),
                ("wrbkh/fs/3972500FL0046", 0),
                ("wrbkh/fs/3972500FL0047", 0),
                ("wrbkh/fs/3972500FL0048", 0),
                ("wrbkh/fs/3972500FL0049", 0),
                ("wrbkh/fs/3972500FL0050", 0),
                ("wrbkh/fs/3972500FL0051", 0),
                ("wrbkh/fs/3972500FL0052", 0),
                ("wrbkh/fs/3972500FL0053", 0),
                ("wrbkh/fs/3972500FL0054", 0),
                ("wrbkh/fs/3972500FL0055", 0),
                ("wrbkh/fs/3972500FL0056", 0),
                ("wrbkh/fs/3972500FL0057", 0),
                ("wrbkh/fs/3972500FL0058", 0),
                ("wrbkh/fs/3972500FL0059", 0),
                ("wrbkh/fs/3972500FL0060", 0),
                ("wrbkh/mobile_pump/3972500MPM0001", 0),
                ("wrbkh/mobile_pump/3972500MPM0002", 0),
                ("wrbkh/mobile_pump/3972500MPM0003", 0),
                ("wrbkh/mobile_pump/3972500MPM0004", 0),
                ("wrbkh/mobile_pump/3972500MPM0005", 0),
                ("wrbkh/mobile_pump/3972500MPM0006", 0),
                ("wrbkh/mobile_pump/3972500MPM0007", 0),
                ("wrbkh/mobile_pump/3972500MPM0008", 0),
                ("wrbkh/mobile_pump/3972500MPM0009", 0),
                ("wrbkh/mobile_pump/3972500MPM0010", 0),
                ("wrbkh/mobile_pump/3972500MPM0011", 0),
                ("wrbkh/mobile_pump/3972500MPM0012", 0),
                ("wrbkh/mobile_pump/3972500MPM0013", 0),
                ("wrbkh/mobile_pump/3972500MPM0014", 0),
                ("wrbkh/mobile_pump/3972500MPM0015", 0),
                ("wrbkh/mobile_pump/3972500MPM0016", 0),
                ("wrbkh/mobile_pump/3972500MPM0017", 0),
                ("wrbkh/mobile_pump/3972500MPM0018", 0),
                ("wrbkh/mobile_pump/3972500MPM0019", 0),
                ("wrbkh/mobile_pump/3972500MPM0020", 0),
                ("wrbkh/mobile_pump/3972500MPM0021", 0),
                ("wrbkh/mobile_pump/3972500MPM0022", 0),
                ("wrbkh/mobile_pump/3972500MPM0023", 0),
                ("wrbkh/mobile_pump/3972500MPM0024", 0),
                ("wrbkh/mobile_pump/3972500MPM0025", 0),
                ("wrbkh/mobile_pump/3972500MPM0026", 0),
                ("wrbkh/mobile_pump/3972500MPM0027", 0),
                ("wrbkh/mobile_pump/3972500MPM0028", 0),
                ("wrbkh/mobile_pump/3972500MPM0029", 0),
                ("wrbkh/mobile_pump/3972500MPM0030", 0),
                ("wrbkh/mobile_pump/3972500MPM0031", 0),
                ("wrbkh/mobile_pump/3972500MPM0032", 0),
                ("wrbkh/mobile_pump/3972500MPM0033", 0),
                ("wrbkh/mobile_pump/3972500MPM0034", 0),
                ("wrbkh/mobile_pump/3972500MPM0035", 0),
                ("wrbkh/mobile_pump/3972500MPM0036", 0),
                ("wrbkh/mobile_pump/3972500MPM0037", 0),
                ("wrbkh/mobile_pump/3972500MPM0038", 0),
                ("wrbkh/mobile_pump/3972500MPM0039", 0),
                ("wrbkh/mobile_pump/3972500MPM0040", 0),
                ("wrbkh/mobile_pump/3972500MPM0041", 0),
                ("wrbkh/mobile_pump/3972500MPM0042", 0),
                ("wrbkh/mobile_pump/3972500MPM0043", 0),
                ("wrbkh/mobile_pump/3972500MPM0044", 0),
                ("wrbkh/mobile_pump/3972500MPM0045", 0),
                ("wrbkh/mobile_pump/3972500MPM0046", 0),
                ("wrbkh/mobile_pump/3972500MPM0047", 0),
                ("wrbkh/mobile_pump/3972500MPM0048", 0),
                ("wrbkh/mobile_pump/3972500MPM0049", 0),
                ("wrbkh/mobile_pump/3972500MPM0050", 0),
                ("wrbkh/mobile_pump/3972500MPM0051", 0),
                ("wrbkh/mobile_pump/3972500MPM0052", 0),
                ("wrbkh/mobile_pump/3972500MPM0053", 0),
                ("wrbkh/mobile_pump/3972500MPM0054", 0),
                ("wrbkh/mobile_pump/3972500MPM0055", 0),
                ("wrbkh/mobile_pump/3972500MPM0056", 0),
                ("wrbkh/mobile_pump/3972500MPM0057", 0),
                ("wrbkh/mobile_pump/3972500MPM0058", 0),
                ("wrbkh/mobile_pump/3972500MPM0059", 0),
                ("wrbkh/mobile_pump/3972500MPM0060", 0),
            ]  # 訂閱資料宣告
            client.subscribe(subscribe_list)  # 批次訂閱
            time.sleep(1)
        except Exception as ee:
            print(str(ee))
