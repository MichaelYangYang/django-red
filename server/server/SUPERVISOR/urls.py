
# -*- coding: utf-8 -*-
from django.urls import path
from django.contrib.auth.decorators import login_required
from SUPERVISOR import views
# from SUPERVISOR.views import API_XXX
urlpatterns = [

    # 頁面公版(一般模式)
    path('home/',views.home),
    path('',views.login),
    path('login/',views.login),
    # # 頁面公版(需登入模式)
    # path('XXX/',login_required(XXX)),

    # API 公版(一般模式)
    path('API_Read_Script/',views.API_Read_Script),
    path('API_Delete_Script/',views.API_Delete_Script),
    path('API_Import_Script/',views.API_Import_Script),
    path('API_MQTT_Monitor_Service_Interface/',views.API_MQTT_Monitor_Service_Interface),
    
    
    # # API 公版(需登入模式)
    # path('API_XXX/',login_required(API_XXX)),
    
]
