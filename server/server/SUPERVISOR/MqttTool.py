import datetime
import time
import paho.mqtt.client as mqtt
import random
import queue
import json
import os

# 讀取設定檔
def Config():
    DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    Config_DIR = os.path.join(DIR, "Config")  # 指定資料庫檔案位置下的目錄
    # print(Dir)
    file_out = open("{}/config.json".format(Config_DIR),"r",newline="")
    config = json.loads(file_out.read())
    file_out.close()
    return config


def start_mqtt():  # 啟動MQTT連線
    # 讀取設定檔
    MQTTConfig = Config()["MQTTServer"]
    User = MQTTConfig["user"]
    Password = MQTTConfig["paswd"]
    Address = MQTTConfig["ip"]
    Port = MQTTConfig["port"]

    DeviceID = Create_DeviceID()  # 隨機創建一組裝置ID
    client = mqtt.Client(DeviceID, False)  # False參數用於記住訂閱的Topic
    client.username_pw_set(User, Password)
    client.connect(Address, Port, keepalive=60)
    client.on_connect = on_connect  # 連線狀態的回調函式
    client.on_message = on_message  # 訂閱有訊息的回調函式
    client.on_disconnect = on_disconnect  # 中斷連線狀態的回調函式
    return client


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))


def on_message(mq, userdata, msg):  # 回調函數，處理訊息內容
    payload = str(msg.payload, encoding="utf-8")
    q.put(payload)
    # print("{}:{}".format(str(msg.topic),str(msg.payload,encoding="utf-8")))


def on_disconnect(client, userdata, rc):
    print("Connected with result code " + str(rc))
    if rc != 0:  # 當連線中斷時
        print("Unexpected disconnection.")


def Create_DeviceID():  #  產生序號
    TestTime = datetime.datetime.now().strftime("%Y%m%d%H%M")
    randomSTR = "".join(random.sample("zyxwvutsrqponmlkjihgfedcba", 3))
    Serial_Number = "{}{}".format(TestTime, randomSTR)  # 序號
    return Serial_Number


q = queue.Queue()  # 設定隊列先進先出


def get_retrain_subscripe(Topic):  # 獲取某個Topic資訊
    client = start_mqtt()  # mqtt連線
    client.on_message = on_message  # 重設回調函式
    client.loop_start()
    client.subscribe(Topic, 0)  # 批次訂閱
    time.sleep(1)
    if q.empty() != True:  # 判斷隊列是否為空值
        msg = q.get()  # 獲取收到的第一條消息並刪除
    return msg
    # client.loop_stop()


def set_publish(Topic, Payload): # 訂閱訊息
    client = start_mqtt()  # mqtt連線
    client.publish(Topic, payload=Payload, qos=0)


if __name__ == "__main__":
    # msg = get_retrain_subscripe("test")
    # print(msg)

    set_publish("test2","is work")
    
