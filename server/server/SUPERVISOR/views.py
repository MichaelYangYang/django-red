from django.shortcuts import render, reverse, redirect
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseRedirect, FileResponse
from django.template import RequestContext
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib import auth
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
import time
import json
import os
import csv
import datetime
import zipfile
import shutil
import random

from SUPERVISOR import MqttTool

# Create your views here.

DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
FileCabinets_DIR = os.path.join(DIR, "FileCabinets")  # 指定資料庫檔案位置下的目錄
Script_DIR = os.path.join(FileCabinets_DIR, "code")  # 指定檔案位置下的目錄

# ----- 登入畫面 ----- #
def login(request):
    try:
        # ---如果使用者為已認證過的用戶則直接登入 ---#
        if request.user.is_authenticated:
            return redirect(reverse(home))

        # ---登入帳戶---#
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        user = auth.authenticate(username=username, password=password)  # 用戶的認證
        if "login" in request.POST:
            print("準備登入")
            if user is not None and user.is_active:  # 如果使用者帳密無誤且為可登入帳戶則直接登入
                auth.login(request, user)  # 保持用戶登入狀態
                if "next" in request.GET:  # 登入後直接前往非法登入前的網頁
                    return redirect(request.GET["next"])
                else:
                    return redirect(reverse(home))  # 前往預設主網頁
            else:
                status_message = "帳密錯誤"
        return render(request, "SUPERVISOR/login.html", locals())
    except Exception as ee:
        print(ee)
        return JsonResponse({"Error": str(ee)})


@login_required
def home(request):
    try:
        data = {}
        # 獲取腳本資料夾的資料夾清單
        Script_List = []
        for row in os.listdir(Script_DIR):
            Script_List.append(row)
        # 查詢對應的檔案路徑
        data["API_Read_Script_URL"] = reverse(API_Read_Script)
        data["API_Delete_Script_URL"] = reverse(API_Delete_Script)
        data["API_Import_Script_URL"] = reverse(API_Import_Script)
        data["API_MQTT_Monitor_Service_Interface_URL"] = reverse(
            API_MQTT_Monitor_Service_Interface
        )

        # 轉換成JSON格式
        json_data = json.dumps(data)

        # POST請求
        if request.method == "POST":
            CodeTitle = request.POST["CodeTitle"]
            if CodeTitle != "":
                code = request.POST["code"]
                # 檢查資料夾，不存在則新增
                if os.path.isdir(os.path.join(Script_DIR, CodeTitle)) != True:
                    os.mkdir(os.path.join(Script_DIR, CodeTitle))
                    # 產生supervisor配置文件內容
                    ini_content = [
                        "[program:{CodeTitle}]\n".format(CodeTitle=CodeTitle),
                        "priority=15\n",
                        "user=root\n",
                        "directory=/FileCabinets/code/{CodeTitle}\n".format(
                            CodeTitle=CodeTitle
                        ),
                        "command=python3 /FileCabinets/code/{CodeTitle}/{CodeTitle}.py\n".format(
                            CodeTitle=CodeTitle
                        ),
                        "autostart=true\n",
                        "startretries=5\n",
                        "redirect_stderr=true\n",
                        "stdout_logfile = /FileCabinets/code/{CodeTitle}/{CodeTitle}.log\n".format(
                            CodeTitle=CodeTitle
                        ),
                        "loglevel=info\n",
                    ]

                    # 寫入檔案
                    file_out = open(
                        "{Script_DIR}/{CodeTitle}/{CodeTitle}.ini".format(
                            Script_DIR=Script_DIR, CodeTitle=CodeTitle
                        ),
                        "w",
                        newline="",
                        encoding="utf-8",
                    )
                    file_out.write("".join(ini_content))
                    file_out.close()
                # 寫入檔案
                file_out = open(
                    "{Script_DIR}/{CodeTitle}/{CodeTitle}.py".format(
                        Script_DIR=Script_DIR, CodeTitle=CodeTitle
                    ),
                    "w",
                    newline="",
                    encoding="utf-8",
                )
                file_out.write(code)
                file_out.close()
                MqttTool.set_publish("DjangoRed/MonitorService/CMD", "update")  # 發佈控制訊息
                # 請求Supervisor Monitor更新
                MqttTool.set_publish(
                    "DjangoRed/MonitorService/CMD", "restart:{}".format(CodeTitle)
                )  # 發佈控制訊息
                

            else:
                pass

        return render(request, "SUPERVISOR/home.html", locals())
    except Exception as ee:
        print(ee)
        return JsonResponse({"Error": str(ee)})


@login_required
# ----- API 讀取腳本內容 ----- #
def API_Read_Script(request):
    try:
        data = {}
        if "script" in request.GET:
            script = request.GET["script"]
            # 讀取檔案
            file_out = open(
                "{Script_DIR}/{script}/{script}.py".format(
                    Script_DIR=Script_DIR, script=script
                ),
                "r",
                newline="",
                encoding="utf-8",
            )

            data["code"] = file_out.read()
            file_out.close()
        return JsonResponse(data)
    except Exception as ee:
        print(ee)
        return JsonResponse({"Error": str(ee)})


@login_required
# ----- API 刪除腳本 ----- #
def API_Delete_Script(request):
    try:
        data = {}
        if "script" in request.GET:
            script = request.GET["script"]

            this_DIR = os.path.join(Script_DIR, script)
            # 讀取資料夾內所有資料
            for row in os.listdir(this_DIR):
                # 批次刪除檔案
                os.remove("{}/{}".format(this_DIR, row))
            # 在一次刪除資料夾樹
            shutil.rmtree(this_DIR)
            # 請求Supervisor Monitor更新
            MqttTool.set_publish("DjangoRed/MonitorService/CMD", "update")  # 發佈控制訊息

        return JsonResponse(data)
    except Exception as ee:
        print(ee)
        return JsonResponse({"Error": str(ee)})


@login_required
# ----- API Monitor介面 ----- #
def API_MQTT_Monitor_Service_Interface(request):
    try:
        data = {}
        if "CMD" in request.GET:
            Public_Topic = "DjangoRed/MonitorService/CMD"
            CMD = request.GET["CMD"]
            # 全部更新
            if CMD == "update":
                MqttTool.set_publish(Public_Topic, CMD)  # 發佈控制訊息
                time.sleep(3)
                msg = MqttTool.get_retrain_subscripe(
                    "MonitorService/DjangoRed/MSG"
                )  # 訂閱MonitorService回應的訊息
                print(msg)
                data["message"] = json.loads(msg)
            # 啟動特定腳本
            if CMD == "start":
                ScriptTitle = request.GET["ScriptTitle"]
                MqttTool.set_publish(
                    Public_Topic, "{}:{}".format(CMD, ScriptTitle)
                )  # 發佈控制訊息
                time.sleep(3)
                msg = MqttTool.get_retrain_subscripe(
                    "MonitorService/DjangoRed/MSG"
                )  # 訂閱MonitorService回應的訊息
                print(msg)
                data["message"] = json.loads(msg)
            # 重啟特定腳本
            if CMD == "restart":
                ScriptTitle = request.GET["ScriptTitle"]
                MqttTool.set_publish(
                    Public_Topic, "{}:{}".format(CMD, ScriptTitle)
                )  # 發佈控制訊息
                time.sleep(3)
                msg = MqttTool.get_retrain_subscripe(
                    "MonitorService/DjangoRed/MSG"
                )  # 訂閱MonitorService回應的訊息
                print(msg)
                data["message"] = json.loads(msg)
            # 暫停特定腳本
            if CMD == "stop":
                ScriptTitle = request.GET["ScriptTitle"]
                MqttTool.set_publish(
                    Public_Topic, "{}:{}".format(CMD, ScriptTitle)
                )  # 發佈控制訊息
                time.sleep(3)
                msg = MqttTool.get_retrain_subscripe(
                    "MonitorService/DjangoRed/MSG"
                )  # 訂閱MonitorService回應的訊息
                print(msg)
                data["message"] = json.loads(msg)
            # 查詢特定腳本運行狀態
            if CMD == "status":
                ScriptTitle = request.GET["ScriptTitle"]
                MqttTool.set_publish(
                    Public_Topic, "{}:{}".format(CMD, ScriptTitle)
                )  # 發佈控制訊息
                time.sleep(3)
                msg = MqttTool.get_retrain_subscripe(
                    "MonitorService/DjangoRed/MSG"
                )  # 訂閱MonitorService回應的訊息
                print(msg)
                data["message"] = json.loads(msg)

        return JsonResponse(data)
    except Exception as ee:
        print(ee)
        return JsonResponse({"Error": str(ee)})


@login_required
# ----- API 匯入腳本 ----- #
def API_Import_Script(request):
    try:
        data = {}
        print(request.POST)
        print(request.FILES)
        print(request.FILES.get('ScriptFile'))
        if request.method == "POST":
            CodeTitle = request.POST["CodeTitle"]
            if "ScriptFile" in request.FILES:
                scriptfile = request.FILES["ScriptFile"]#從請求中獲取檔案
                file_out = open(
                        "{Script_DIR}/{CodeTitle}/{CodeTitle}.py".format(
                            Script_DIR=Script_DIR, CodeTitle=CodeTitle
                        ),
                        "wb+",
                        # newline="",
                        # encoding="utf-8",
                    )
                for chunk in scriptfile.chunks(): 
                    file_out.write(chunk)
                    file_out.close()
                data["message"] = "OK"
            else:
                data["message"] = "NoFile"
        print(data)
            
            

        return JsonResponse(data)
    except Exception as ee:
        print(ee)
        return JsonResponse({"Error": str(ee)})

# ----- request公版 ----- #
# def XXX(request):
#     try:
#         return render(request, 'SUPERVISOR/XXX.html', locals())
#     except Exception as ee:
#         print(ee)
#         return JsonResponse({"Error":str(ee)})

# # ----- API公版 ----- #
# def API_XXX(request):
#     try:
#         data = {}
#         return JsonResponse(data)
#     except Exception as ee:
#         print(ee)
#         return JsonResponse({"Error":str(ee)})
