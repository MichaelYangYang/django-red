import os
# 檢查檔案路徑
DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
FileCabinets_DIR = os.path.join(DIR, "FileCabinets")#指定資料庫檔案位置下的目錄
Script_DIR = os.path.join(FileCabinets_DIR,"code")#指定檔案位置下的目錄
print("/FileCabinets/code is {}".format(os.path.isdir(Script_DIR)))
if os.path.isdir(Script_DIR) != True:
    # 產生資料夾
    os.mkdir(Script_DIR)