$(document).ready(function () {
    create_editor(); // 建立ace編輯器

});

var editor;
// 建立ace編輯器
function create_editor() {
    editor = ace.edit("CodePanel");
    // editor.setReadOnly(true); // 不允許寫入
    editor.setTheme("ace/theme/tomorrow_night_eighties"); // 模板(列表[註2])
    editor.session.setMode("ace/mode/python"); // 預設語言模式(列表[註3])
    // 字體大小設定
    document.getElementById('CodePanel').style.fontSize = '14px';
}

// 搜尋功能
function search() {
    editor.find(document.getElementById("keyword").value, {
        // 搜尋參數設定[註1]
        backwards: false, // 從底部往上搜尋
        wrap: false, // 當搜尋至底部時，則返回頂端
        caseSensitive: false, // 大小寫要完全符合
        wholeWord: false, // 單字要完全符合
        regExp: false // 正規表達式
    });
    editor.findNext();
    editor.findPrevious();
}



// 觸發所選的腳本名稱
function select_script() {
    var sel_script = $("#sel_script").val().toString();
    $("#Script_name").text(sel_script);// 變更顯示的腳本名稱
    $("#CodeTitle").val(sel_script);// 變更表單的腳本名稱
    $("#import_script_name").val(sel_script);// 變更import model的腳本名稱
    download(sel_script)// download read script
    // 顯示隱藏的功能
    show_function(); // show behide function
}

// show behide function
function show_function() {
    $("#delete_script").show();
    $("#import_script").show();
    $("#run_script_function").show();
}

// upload
function upload() {
    // 獲取文字後寫入textarea輸入框
    var CodeContent = editor.getValue();
    console.log(CodeContent)
    $("#CodeContent").val(CodeContent);
    // 獲取表單資料
    var formdata = new FormData($("#uploadForm")[0]);
    $.ajax({
        type: "post",
        url: "",
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
        success: function (response) {
            alert("successful!")
        }
    });
}

// download
function download(script) {
    var json_data = $("#json_data").text();
    var data = JSON.parse(json_data);

    // 獲取文字
    $.ajax({
        type: "get",
        url: data["API_Read_Script_URL"],
        data: {
            script: script,
        },
        cache: true,
        success: function (response) {
            // 寫入新值
            editor.setValue(response["code"], 1);
        }
    });
}

// new script
function new_script() {
    var script = prompt("New script name:", "");
    if (script != null && script != "") {
        $("#Script_name").text(script);// 變更顯示的腳本名稱
        $("#CodeTitle").val(script);// 變更表單的腳本名稱
        // 寫入新值
        editor.setValue("# Date:" + new Date() + "\n# Put code on here...\n", 1);
    }
}

// delete script
function delete_script() {
    script = $("#CodeTitle").val();
    var json_data = $("#json_data").text();
    var data = JSON.parse(json_data);
    var r = confirm("確認是否刪除'" + script + "'腳本?");
    if (r == true) {
        // 刪除
        $.ajax({
            type: "get",
            url: data["API_Delete_Script_URL"],
            data: {
                script: script,
            },
            cache: true,
            success: function (response) {
                // 寫入新值
                alert("Delete successful !!")
                window.location.reload()
            }
        });
    }
    else {

    }



}

// import script
function import_script() {
    // 獲取表單資料
    var json_data = $("#json_data").text();
    var data = JSON.parse(json_data);
    var formdata = new FormData($("#Import_Script_Form")[0]);
    console.log(formdata)
    $.ajax({
        type: "POST",
        url: data["API_Import_Script_URL"],
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
        success: function (response) {

            if (response.message == "OK") {
                alert("successful!")
                window.location.reload()
            }
            else if (response.message == "NoFile") {
                alert("No select file!")
            }
            else {
                alert(response.message)
            }

        }
    });
}

// run script
function run_script(cmd) {
    var json_data = $("#json_data").text();
    var data = JSON.parse(json_data);
    $.ajax({
        type: "get",
        url: data["API_MQTT_Monitor_Service_Interface_URL"],
        data: {
            "CMD": cmd,
            "ScriptTitle": $("#CodeTitle").val(),
        },
        success: function (response) {
            console.log(response)
        }
    });
}




function test() {
    alert("is work")

}