import subprocess
import os
import socket
import shutil
BASE_DIR = os.path.dirname(os.path.abspath(__file__))  # 抓取目前檔案位置

# DIR = os.path.join(BASE_DIR, "FileCabinets/ServerIP")#指定檔案位置下的目錄


def runLocalServer():  # 開啟伺服器
    print(BASE_DIR)
    ourIP = "127.0.0.1"
    print("伺服器位置={}".format(ourIP))
    ip_address = input("請輸入Port位置：")
    subprocess.run("python {}\manage.py runserver {}:{}".format(
        BASE_DIR, ourIP, ip_address))


def runserver():  # 開啟伺服器
    print(BASE_DIR)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ourIP = s.getsockname()[0]
    s.close()
    print("伺服器位置={}".format(ourIP))
    ip_address = input("請輸入Port位置：")
    subprocess.run("python {}\manage.py runserver {}:{}".format(
        BASE_DIR, ourIP, ip_address))


def check():  # 檢查資料庫模型
    subprocess.run("python manage.py check")


def makemigrations():  # 更新資料庫版本
    app = input("請輸入App名稱：")
    subprocess.run('python manage.py makemigrations {}'.format(app))  # 回傳字串值


def sqlmigrate():  # 產生SQL Table字串
    app = input("請輸入App名稱：")
    version = input("請輸入版本：")
    subprocess.run('python manage.py sqlmigrate {} {}'.format(
        app, version))  # 回傳字串值


def migrate():  # 資料庫同步
    subprocess.run('python manage.py migrate')  # 回傳字串值


def migrateApp():  # 資料庫同步對特定App
    app = input("請輸入App名稱：")
    version = input("請輸入版本：")
    subprocess.run('python manage.py migrate {} {}'.format(
        app, version))  # 回傳字串值


def ipconfig():  # 查詢本機ip位置
    subprocess.run('ipconfig')  # 回傳字串值


def startapp():  # 創建新的app
    print(BASE_DIR)
    app = input("請輸入App名稱：")
    subprocess.run('python manage.py startapp {}'.format(app))  # 回傳字串值
    # 讀取主專案名稱
    MainProjectName = BASE_DIR.split("\\")[-1]
    
    # 修改view.py檔案
    file_out = open("{}/{}/views.py".format(BASE_DIR,app),"w",encoding="utf-8")
    file_out.write(
'''
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseRedirect, FileResponse
from django.template import RequestContext
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib import auth
from django.http import JsonResponse
import time
import json
import os
import csv
import datetime
import zipfile
import shutil
import random
# Create your views here.
''')
    file_out.write(
'''
# ----- request公版 ----- #
def XXX(request):
    try:
        return render(request, '%(app_name)s/XXX.html', locals())
    except Exception as ee:
        print(ee)
        return JsonResponse({"Error":str(ee)})
'''%({"app_name":app}))

    file_out.write(
"""
# ----- API公版 ----- #
def API_XXX(request):
    try:
        data = {}
        return JsonResponse(data)
    except Exception as ee:
        print(ee)
        return JsonResponse({"Error":str(ee)})

""")

    
    file_out.close()
    print("在{}資料夾中修改views.py檔案完成...".format(app))

    # 生成urls.py檔案
    file_out = open("{}/{}/urls.py".format(BASE_DIR,app),"w",encoding="utf-8")
    file_out.write(
'''
# -*- coding: utf-8 -*-
from django.urls import path
from django.contrib.auth.decorators import login_required
from %(app_name)s.views import XXX
from %(app_name)s.views import API_XXX
urlpatterns = [

    # 頁面公版(一般模式)
    path('XXX/',XXX),
    # 頁面公版(需登入模式)
    path('XXX/',login_required(XXX)),

    # API 公版(一般模式)
    path('API_XXX/',API_XXX),
    # API 公版(需登入模式)
    path('API_XXX/',login_required(API_XXX)),
    
]
'''%({"app_name":app}))
    file_out.close()
    print("在{}資料夾中生成urls.py檔案完成...".format(app))

    # 在media資料夾中產生app資料夾 
    os.mkdir("{}/media/{}".format(BASE_DIR,app))
    print("在media資料夾中產生{}資料夾完成...".format(app))

    # 在static資料夾中產生app資料夾
    os.mkdir("{}/static/{}".format(BASE_DIR,app))
    print("在static資料夾中產生{}資料夾完成...".format(app))
    
    # 在templates資料夾中產生app資料夾
    os.mkdir("{}/templates/{}".format(BASE_DIR,app))
    print("在templates資料夾中產生{}資料夾完成...".format(app))

    
    # 在主資料夾的settings.py中加入app
    file_out = open("{}/{}/settings.py".format(BASE_DIR,MainProjectName),"r",encoding="utf-8")
    context = file_out.readlines()
    file_out.close()
    # __修改文件
    for index in range(0,len(context)):
        # 修改位置：INSTALLED_APPS
        if context[index].find("INSTALLED_APPS") == 0:
            # print(context[index].find("INSTALLED_APPS"))
            print(context[index])
            context.insert(index+1,"    '{}',\n".format(app))
    # __回寫文件
    file_write = open("{}/{}/settings.py".format(BASE_DIR,MainProjectName),"w",encoding="utf-8",newline="")
    for row in context:
        file_write.write(row)
    file_write.close()
    print("修改{}/settings.py中加入{}的連結完成...".format(MainProjectName,app))
    
    
    # 在主資料夾的urls.py中加入app/urls.py的連結
    file_out = open("{}/{}/urls.py".format(BASE_DIR,MainProjectName),"r",encoding="utf-8")
    context = file_out.readlines()
    file_out.close()
    # __修改文件
    for index in range(0,len(context)):
        # 修改位置：urlpatterns
        if context[index].find("urlpatterns") == 0:
            # print(context[index].find("urlpatterns"))
            print(context[index])
            context.insert(index+1,"    path(\"\", include(\"{}.urls\")),\n".format(app))
    
    # __回寫文件
    file_write = open("{}/{}/urls.py".format(BASE_DIR,MainProjectName),"w",encoding="utf-8",newline="")
    for row in context:
        file_write.write(row)
    file_write.close()
    print("修改{}/urls.py中加入{}/urls.py的連結完成...".format(MainProjectName,app))



def startProject():  # 創建django專案
    # 讀取資料夾位置
    DIR= os.path.dirname(os.path.abspath(__file__))
    
    # 創建django專案
    project = input("請輸入專案名稱：")
    subprocess.Popen("django-admin startproject {}".format(project))
    stay = input("專案產生正確產生請按Enter繼續...")
    Django_DIR = os.path.join(DIR, project)#指定資料庫檔案位置下的目錄
    print(DIR)
    # 產生需要的資料夾
    os.mkdir("FileCabinets")
    os.mkdir("{}/{}/templates".format(DIR,project))
    os.mkdir("{}/static".format(Django_DIR))
    os.mkdir("{}/media".format(Django_DIR))
    
    # 修改urls.py文件
    file_out = open("{}/{}/urls.py".format(project,project),"r",encoding="utf-8")
    context = file_out.readlines()
    file_out.close()
    # __修改文件
    for index in range(0,len(context)):
        # 修改位置：from django.urls import path
        if context[index].find("from django.urls import path") == 0:
            # print(context[index].find("from django.urls import path"))
            # print(context[index])
            context.insert(index+1,"from django.urls import include\n")
    # __回寫文件
    file_write = open("{}/{}/urls.py".format(project,project),"w",encoding="utf-8",newline="")
    for row in context:
        file_write.write(row)
    file_write.close()
    print("修改{}/urls.py檔案完成...".format(project))

    # 修改settings.py文件
    file_out = open("{}/{}/settings.py".format(project,project),"r",encoding="utf-8")
    context = file_out.readlines()
    file_out.close()
    # __修改文件
    for index in range(0,len(context)):

        # 修改位置：BASE_DIR
        if context[index].find("BASE_DIR") == 0:
            text = [
                "DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))\n",
                "SQL_DIR = os.path.join(DIR, \"FileCabinets\")#指定資料庫檔案位置下的目錄\n"
            ]
            context.insert(index+1,''.join(text))
            print("指定資料庫與資料櫃生成位置配置完成...")
        
        # 修改位置：ALLOWED_HOSTS
        if context[index].find("ALLOWED_HOSTS") == 0:
            text = [
                "ALLOWED_HOSTS = ['*']\n",
            ]
            context[index] = ''.join(text)
            print("允許所有HOST使用配置完成...")

        # 修改位置：TEMPLATES
        if context[index].find("TEMPLATES = [") == 0:
            text = [
                "        'DIRS': [os.path.join(BASE_DIR,'templates')],\n",
            ]
            context[index+3] = ''.join(text)
            print("新增templates位置配置完成...")

        # 修改位置：DATABASES
        if context[index].find("DATABASES = {") == 0:
            text = [
                "        'NAME': os.path.join(SQL_DIR, 'db.sqlite3'),\n",
            ]
            context[index+3] = ''.join(text)
            print("修改SQLite檔案位置配置完成...")

        # 修改位置：LANGUAGE_CODE
        if context[index].find("LANGUAGE_CODE") == 0:
            text = [
                "LANGUAGE_CODE = 'zh-hant'\n",
            ]
            context[index] =''.join(text)
            print("修改語言配置完成...")

        # 修改位置：TIME_ZONE
        if context[index].find("TIME_ZONE") == 0:
            text = [
                "TIME_ZONE = 'Asia/Taipei'\n",
            ]
            context[index] =''.join(text)
            print("修改時區配置完成...")

        

    # __新增位置：static位置配置
    text = [
        "STATIC_URL = '/static/'\n",
        "STATICFILES_DIRS=[\n",
        "    os.path.join(BASE_DIR,\"static\"),\n"
        "    os.path.join(DIR,\"FileCabinets\"),\n"
        "]\n"
    ]
    context.append(''.join(text))
    print("新增static位置配置完成...")

    # __新增位置：media位置位置
    text = [
        "MEDIA_URL = '/media/'\n",
        "MEDIAFILES_DIRS=[\n",
        "    os.path.join(BASE_DIR, \"media\"),\n"
        "]\n"
    ]
    context.append(''.join(text))
    print("新增media位置配置完成...")        
    
    # __新增位置：登入URL配置
    text = [
        "LOGIN_URL = '/'\n",
    ]
    context.append(''.join(text))
    print("新增登入URL配置完成...")

    # __新增位置：Session功能配置
    text = [
        "SESSION_COOKIE_AGE = 60*30#設置session過期時間為30分鐘\n",
        "SESSION_EXPIRE_AT_BROWSER_CLOSE = True#當瀏覽器被關閉的時候將session失效，但是不能刪除數據庫的session數據\n",
        "SESSION_SAVE_EVERY_REQUEST = True#每次請求都要保存一下session\n",
        "SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'\n",
    ]
    context.append(''.join(text))
    print("新增Session功能配置完成...")

    # __回寫文件
    file_write = open("{}/{}/settings.py".format(project,project),"w",encoding="utf-8",newline="")
    for row in context:
        file_write.write(row)
    file_write.close()
    print("修改{}/settings.py檔案完成...".format(project))
    
    # 將Auto_runserver.py複製到資料夾內
    shutil.copy("Auto_runserver.py","{}/Auto_runserver.py".format(project))
    
    


def SuperAdmin():  # 創建超級使用者
    subprocess.run('python manage.py createsuperuser')  # 回傳字串值

# 產生此專案的docker-compose文件
def creat_DockerCompose(RepositoryName):
    # 讀取上層資料夾位置
    Base_Dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # 讀取上層資料夾名稱
    Base_Dir_Name = Base_Dir.split("\\")[-1]
    # 讀取此專案名稱
    Project_Name = os.path.dirname(os.path.abspath(__file__)).split("\\")[-1]
    # print(Project_Name)
    # print(Base_Dir)

    # 產生文件：django用的dockerfile
    # __文件內容
    context = [
        "FROM python:3.6\n",
        "LABEL iotservertest web\n",
        "ENV PYTHONUNBUFFERED 1\n",
        "RUN mkdir /{}\n".format(Project_Name),
        "RUN mkdir /FileCabinets\n",
        "COPY . /{}\n".format(Project_Name),
        "WORKDIR /{}  \n".format(Project_Name),
        "RUN pip install -r allpackage.txt\n",
    ]
    # __寫入文件
    file_out = open("{}/{}/dockerfile".format(Base_Dir,Project_Name),"w",newline="",encoding="utf-8")
    file_out.write("".join(context))
    file_out.close()
    print("產生文件：django用的dockerfile")
    

    # 產生文件：django用的allpackage.txt
    context = [
        "Django==2.1.5\n",
        "gunicorn==19.9.0\n",
        "gevent==1.2.2\n",
        "channels==2.2.0\n",
        "channels-redis==2.4.0\n",
        "paho-mqtt==1.4.0\n",
        "requests==2.21.0\n",
        "pymongo==3.8.0\n",
    ]
    # __寫入文件
    file_out = open("{}/{}/allpackage.txt".format(Base_Dir,Project_Name),"w",newline="",encoding="utf-8")
    file_out.write("".join(context))
    file_out.close()
    print("產生文件：django用的allpackage.txt")

    # 產生文件：docker-compose.yml
    context = [
        "version: \'3\'\n",
        "services:\n",
        "    djangoweb:\n",
        "        container_name: django-webtest \n",
        "        build: ./{} \n".format(Project_Name),
        "        restart: always \n",
        "        expose: \n",
        "        - \"8000\"\n",
        "        volumes:\n",
        "        - ./FileCabinets:/FileCabinets\n",
        "        - ./{}:/{}\n".format(Project_Name,Project_Name),
        "        command: bash -c \"gunicorn -w 8 --timeout 600 --bind 0.0.0.0:8000 {}.wsgi\"\n".format(Project_Name),
        "        environment:\n",
        "        - TZ=Asia/Taipei \n",
        "        networks:\n",
        "        - nginx_network\n",
        "    nginx:\n",
        "        container_name: nginx-webtest    \n",   
        "        build: ./ \n",
        "        restart: always \n",
        "        ports: \n",
        "        - \"80:80\" \n",
        "        volumes:\n",
        "        - ./FileCabinets:/FileCabinets\n",
        "        depends_on:\n",
        "        - djangoweb\n",
        "        environment:\n",
        "        - TZ=Asia/Taipei \n",
        "        networks:\n",
        "        - nginx_network\n",
        "networks:\n",
        "    nginx_network:\n",
        "        driver: bridge\n",
    ]
    # __寫入文件
    file_out = open("{}/docker-compose.yml".format(Base_Dir),"w",newline="",encoding="utf-8")
    file_out.write("".join(context))
    file_out.close()
    print("產生文件：docker-compose.yml")

    # 產生資料夾：nginx
    os.mkdir("{}/nginx".format(Base_Dir))
    print("產生資料夾：nginx")

    # 產生文件：nginx用的mydjango.conf
    context = [
        "upstream web {\n",
        "  ip_hash;\n",
        "  server djangoweb:8000;\n",
        "}\n",
        "server {\n",
        "  listen 80;\n",
        "  server_name localhost;\n",
        "  location / {\n",
        "    proxy_pass http://web/;\n",
        "  }\n",
        "  location /static/ {\n",
        "    alias /static/;\n",
        "  }\n",
        "  location /FileCabinets/ {\n",
        "    alias /FileCabinets/;\n",
        "  }\n",
        "}\n",
    ]
    # __寫入文件
    file_out = open("{}/nginx/mydjango.conf".format(Base_Dir),"w",newline="",encoding="utf-8")
    file_out.write("".join(context))
    file_out.close()
    print("產生文件：nginx用的mydjango.conf")

    # 產生文件：nginx用的nginx.conf
    context = [
        "user  root;\n",
        "worker_processes  1;\n",
        "error_log  /var/log/nginx/error.log warn;\n",
        "pid        /var/run/nginx.pid;\n",
        "events {\n",
        "    worker_connections  1024;\n",
        "}\n",
        "http {\n",
        "    include       /etc/nginx/mime.types;\n",
        "    default_type  application/octet-stream;\n",
        "    log_format  main  \'$remote_addr - $remote_user [$time_local] \"$request\" \'\n",
        "                    \'$status $body_bytes_sent \"$http_referer\" \'\n",
        "                    \'\"$http_user_agent\" \"$http_x_forwarded_for\"\';\n",
        "    access_log  /var/log/nginx/access.log  main;\n",
        "    sendfile        on;\n",
        "    keepalive_timeout  600;\n",
        "    proxy_connect_timeout   600;\n",
        "    proxy_send_timeout      600;\n",
        "    proxy_read_timeout      600;\n",
        "    include /etc/nginx/conf.d/*.conf;\n",
        "}\n",
    ]
    # __寫入文件
    file_out = open("{}/nginx/nginx.conf".format(Base_Dir),"w",newline="",encoding="utf-8")
    file_out.write("".join(context))
    file_out.close()
    print("產生文件：nginx用的nginx.conf")

    # 產生文件：nginx用的dockerfile
    context = [
        "FROM nginx:latest\n",
        "RUN mkdir /static\n",
        "COPY {}/static /static\n".format(Project_Name),
        "RUN mkdir /FileCabinets\n",
        "COPY nginx/nginx.conf /etc/nginx\n",
        "RUN rm /etc/nginx/conf.d/default.conf\n",
        "COPY nginx/mydjango.conf /etc/nginx/conf.d\n",
        "CMD [\"nginx\", \"-g\", \"daemon off;\"]\n",
    ]
    # __寫入文件
    file_out = open("{}/dockerfile".format(Base_Dir),"w",newline="",encoding="utf-8")
    file_out.write("".join(context))
    file_out.close()
    print("產生文件：nginx用的dockerfile")

    # 產生文件：發佈指令.txt
    Tag_Name = input("請輸入導入Docker Hub的Tag Name：")
    if Tag_Name == "":
        print("不得為空值，Tag_Name將預設為：{}".format(Project_Name))
        Tag_Name = Project_Name
    
    context = [
        "Tag_Name = {}\n".format(Tag_Name),
        "{}:{}_djangoweb\n".format(RepositoryName,Tag_Name),
        "{}:{}_nginx\n".format(RepositoryName,Tag_Name),
        "//登入\n",
        "docker login\n",
        "//上傳，請先確認是否能在本機端docker-compose up運行\n",
        "docker-compose build\n",
        "docker tag {}_nginx {}:{}_nginx\n".format(Base_Dir_Name,RepositoryName,Tag_Name),
        "docker push {}:{}_nginx\n".format(RepositoryName,Tag_Name),
        "docker tag {}_djangoweb {}:{}_djangoweb\n".format(Base_Dir_Name,RepositoryName,Tag_Name),
        "docker push {}:{}_djangoweb\n".format(RepositoryName,Tag_Name),
        "//登出\n",
        "docker logout {}\n".format(RepositoryName.split("/")[0]),
    ]
    # __寫入文件
    file_out = open("{}/發佈指令.txt".format(Base_Dir),"w",newline="",encoding="utf-8")
    file_out.write("".join(context))
    file_out.close()
    print("產生文件：發佈指令.txt")

def Auto_build_docker(RepositoryName):
    # 讀取上層資料夾位置
    Base_Dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # 讀取上層資料夾名稱
    Base_Dir_Name = Base_Dir.split("\\")[-1]
    # 讀取此專案名稱
    Project_Name = os.path.dirname(os.path.abspath(__file__)).split("\\")[-1]
    # 導入Docker Hub的Tag Name
    Tag_Name = input("請輸入導入Docker Hub的Tag Name：")
    if Tag_Name == "":
        input("不得為空值,請按任意鍵繼續...")
    else:
        # 修改setting.py文件
        # __讀取settings.py文件
        context = open("{}/{}/{}/settings.py".format(Base_Dir,Project_Name,Project_Name),"r",encoding="utf-8",newline="").readlines()
        # __修改DEBUG參數
        
        for index in range(0,len(context)):
            # 修改位置：DEBUG
            if context[index].find("DEBUG") == 0:
                # print(context[index].find("urlpatterns"))
                print(context[index])
                context[index] = "DEBUG = False\n"
                print("修改參數DEBUG = False")
        
        # __回寫文件
        file_write = open("{}/{}/{}/settings.py".format(Base_Dir,Project_Name,Project_Name),"w",encoding="utf-8",newline="")
        for row in context:
            file_write.write(row)
        file_write.close()
        
        print("修改settings.py檔案完成...")
        # 建置專案
        subprocess.run("docker-compose build")
        print("建置專案完成...")
        # 發佈至docker hub
        subprocess.run("docker tag {}_nginx {}:{}_nginx".format(Base_Dir_Name,RepositoryName,Tag_Name))
        subprocess.run("docker tag {}_djangoweb {}:{}_djangoweb".format(Base_Dir_Name,RepositoryName,Tag_Name))
        subprocess.run("docker push {}:{}_nginx".format(RepositoryName,Tag_Name))
        subprocess.run("docker push {}:{}_djangoweb".format(RepositoryName,Tag_Name))
        print("發佈至Docker Hub完成...")
    


# Auto_build_docker()

while True:
    try:
        # os.system("cls")  # 清除輸入介面
        context = [
            "************************\n",
            "-1.查詢ip位置\n",
            "0.開啟本地端伺服器\n",
            "1.開啟伺服器\n",
            "2.檢查資料庫模型\n",
            "3.更新資料庫版本\n",
            "4.產生SQL Table字串\n",
            "5.資料庫同步\n",
            "6.資料庫同步對特定App\n",
            "30.佈署至Docker Hub\n",
            "51.創建django專案\n",
            "52.創建新的app\n",
            "53.創建超級使用者\n",
            "54.創建docker佈署文件\n",
            "99.結束程式\n",
            "************************\n",
        ]
        print("".join(context))
        enter = int(input("請輸入執行代碼："))
        if enter == -1:
            ipconfig()
            input("Enter返回主選單")
        if enter == 0:
            runLocalServer()
            input("Enter返回主選單")
        elif enter == 1:
            runserver()
            input("Enter返回主選單")
        elif enter == 2:
            check()
            input("Enter返回主選單")
        elif enter == 3:
            makemigrations()
            input("Enter返回主選單")
        elif enter == 4:
            sqlmigrate()
            input("Enter返回主選單")
        elif enter == 5:
            migrate()
            input("Enter返回主選單")
        elif enter == 6:
            migrateApp()
            input("Enter返回主選單")
        elif enter == 30:
            RepositoryName = input("請輸入帳戶/儲存庫名稱(Ex：michael/system)：")
            Auto_build_docker(RepositoryName)
            input("Enter返回主選單")   
        elif enter == 51:
            startProject()
            input("Enter返回主選單")
        elif enter == 52:
            startapp()
            input("Enter返回主選單")
        elif enter == 53:
            SuperAdmin()
            input("Enter返回主選單")
        elif enter == 54:
            RepositoryName = input("請輸入帳戶/儲存庫名稱(Ex：michael/system)：")
            creat_DockerCompose(RepositoryName)
            input("Enter返回主選單")
        elif enter == 99:  # 結束程式
            break
        else:
            print("錯誤代碼，請重新輸入!!")
    except Exception as ee:
        print(ee)
